# C Linked List

My implementation of a doubly linked list in C language. It should work for all basic data types.

## Description
Doubly linked list is a dynamic data structure used to store data in a way where each node has a pointer pointing to 
the next and previous element of a list.

## Structure
This list is a structure that contains pointers to the first and last node, number of items and size of data type 
stored.
```C
typedef struct
{
    linkedListNode_t* head;
    linkedListNode_t* tail;
    uint32_t size;
    size_t data_size;
} linkedList_t;
```

Each node contains a value and pointers to the previous and next element.
```C
typedef struct linkedListNode_t linkedListNode_t;
struct linkedListNode_t
{
    void* value;
    linkedListNode_t* next;
    linkedListNode_t* prev;
};
```

## Usage
There are 4 main types of functions used to manipulate the list:
#### Adding an element
- `bool linked_list_insert_last(linkedList_t *list, const void *element)`
  + Puts an `element` to the end of a `list`
- `bool linked_list_insert_first(linkedList_t *list, const void *element)`
  + Puts an `element` to the beginning of a `list`
- `bool linked_list_insert_at(linkedList_t *list, const void *element, uint32_t position)`
  + Puts an `element` to the specific `position` of a `list`

  All of the above return `true` if element is added successfully.

#### Removing an element
- `bool linked_list_remove_last(linkedList_t *list)`
  + Removes an `element` from the end of a `list`
- `bool linked_list_remove_first(linkedList_t *list)`
  + Removes an `element` from the beginning of a `list`
- `bool linked_list_remove_element(linkedList_t *list, const void *element)`
  + Removes the first occurrence of an `element` from the `list`
- `bool linked_list_remove_at(linkedList_t *list, uint32_t position)`
  + Removes an element from a specific `position` of the `list`

  All of the above return `true` if element is removed successfully.

#### Getting an element
- `void *linked_list_get_first(const linkedList_t *list)`
  + Returns the first element of a `list` or `NULL` if a list is empty
- `void *linked_list_get_last(const linkedList_t *list)`
  + Returns the last element of a `list` or `NULL` if a list is empty
- `void *linked_list_get_at(const linkedList_t *list, uint32_t position)`
  + Returns the element in a specific `position` of a `list` or
  + Returns `NULL` if `position` is invalid or if a `list` is empty

#### Other functions
- `linkedList_t *linked_list_create(size_t size)`
  + Creates a new linked list
  + `size` is size of a data type that will be stored in this list
  + Returns a pointer to the list or `NULL` if a list cannot be created
- `void linked_list_destroy(linkedList_t *list)`
  + Delete the `list` and its content from memory

## Example
Here is an example showing how the list should be used.
```C
#include "linkedList.h"

int main()
{
    int a;
    linkedList_t *ll = linked_list_create(sizeof(int)); // ll = {}

    if (ll == NULL) return 1;

    /* it is assumed that every operation was successfull after this point */

    a = 1;
    linked_list_insert_last(ll, &a); // ll = {1}
    a = 2;
    linked_list_insert_last(ll, &a); // ll = {1,2}
    a = 3;
    linked_list_insert_first(ll, &a); // ll = {3,1,2}
    a = 4;
    linked_list_insert_at(ll, &a, 1); // ll = {3,4,1,2}

    a = *(int *) linked_list_get_last(ll); // a = 2
    a = *(int *) linked_list_get_at(ll, 2); // a = 1

    linked_list_remove_first(ll); // ll = {4,1,2}
    linked_list_remove_element(ll, &a); // ll = {4,2}
    linked_list_remove_at(ll, 0); //ll = {2}

    linked_list_destroy(ll);
    return 0;
}
```

## License
Everyone is free to use this code as they wish, albeit at their own risk.
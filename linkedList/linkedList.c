#include <stdlib.h>
#include <string.h>
#include "linkedList.h"

bool linked_list_insert_last(linkedList_t *list, const void *element)
{
    if ((list == NULL) || (element == NULL))
    {
        return false;
    }

    linkedListNode_t *node = (linkedListNode_t *) malloc(sizeof(linkedListNode_t));

    if (node == NULL)
    {
        return false;
    }

    node->next = NULL;
    node->prev = list->tail;
    node->value = malloc(list->data_size);

    if (node->value == NULL)
    {
        free(node);
        return false;
    }

    memcpy(node->value, element, list->data_size);

    if (list->size == 0)
    {
        list->head = node;
    }
    else
    {
        list->tail->next = node;
    }
    list->tail = node;

    list->size++;

    return true;
}

bool linked_list_insert_first(linkedList_t *list, const void *element)
{
    if ((list == NULL) || (element == NULL))
    {
        return false;
    }

    linkedListNode_t *node = (linkedListNode_t *) malloc(sizeof(linkedListNode_t));

    if (node == NULL)
    {
        return false;
    }

    node->next = list->head;
    node->prev = NULL;
    node->value = malloc(list->data_size);

    if (node->value == NULL)
    {
        free(node);
        return false;
    }

    memcpy(node->value, element, list->data_size);

    if (list->size == 0)
    {
        list->tail = node;
    }
    else
    {
        list->head->prev = node;
    }
    list->head = node;

    list->size++;

    return true;
}

bool linked_list_insert_at(linkedList_t *list, const void *element, uint32_t position)
{
    if ((list == NULL) || (element == NULL))
    {
        return false;
    }

    linkedListNode_t *tmp = NULL;
    linkedListNode_t *node = (linkedListNode_t *) malloc(sizeof(linkedListNode_t));

    if (node == NULL)
    {
        return false;
    }

    if (position == 0)
    {
        linked_list_insert_first(list, element);
    }
    else if (position == list->size)
    {
        linked_list_insert_last(list, element);
    }
    else if (position > list->size)
    {
        return false;
    }
    else
    {
        node->next = NULL;
        node->prev = NULL;
        node->value = malloc(list->data_size);

        if (node->value == NULL)
        {
            free(node);
            return false;
        }

        memcpy(node->value, element, list->data_size);

        tmp = list->head;
        for (uint32_t i = 0; i < position; i++)
        {
            tmp = tmp->next;
        }

        node->prev = tmp->prev;
        node->next = tmp;
        tmp->prev->next = node;
        tmp->prev = node;

        list->size++;
    }
    return true;
}

bool linked_list_remove_last(linkedList_t *list)
{
    if ((list == NULL) || (list->size == 0))
    {
        return false;
    }

    linkedListNode_t *newLast = NULL;

    if (list->size == 1)
    {
        free(list->tail->value);
        free(list->tail);
        list->head = NULL;
        list->tail = NULL;
    }
    else
    {
        newLast = list->tail->prev;
        newLast->next = NULL;
        free(list->tail->value);
        free(list->tail);
        list->tail = newLast;
    }

    list->size--;
    return true;
}

bool linked_list_remove_first(linkedList_t *list)
{
    if ((list == NULL) || (list->size == 0))
    {
        return false;
    }

    linkedListNode_t *newFirst = NULL;

    if (list->size == 1)
    {
        free(list->head->value);
        free(list->head);
        list->head = NULL;
        list->tail = NULL;
    }
    else
    {
        newFirst = list->head->next;
        newFirst->prev = NULL;
        free(list->head->value);
        free(list->head);
        list->head = newFirst;
    }

    list->size--;
    return true;
}

bool linked_list_remove_element(linkedList_t *list, void *element)
{
    if ((list == NULL) || (element == NULL) || (list->size == 0))
    {
        return false;
    }

    linkedListNode_t *node = list->head;
    uint32_t pos = 1;

    while (node != NULL)
    {
        // check if values are different
        if (memcmp(node->value, element, list->data_size) != 0)
        {
            node = node->next;
            pos++;
            continue;
        }
        // edge cases
        if (pos == 1)
        {
            return linked_list_remove_first(list);
        }
        else if (pos == list->size)
        {
            return linked_list_remove_last(list);
        }
        // general case
        else
        {
            node->next->prev = node->prev;
            node->prev->next = node->next;
            free(node->value);
            free(node);
            list->size--;
        }

        return true;
    }

    return false;
}

bool linked_list_remove_at(linkedList_t *list, uint32_t position)
{
    linkedListNode_t *node = NULL;

    if ((list == NULL) || (list->size == 0) || (position >= list->size))
    {
        return false;
    }
    else if (position == 0)
    {
        return linked_list_remove_first(list);
    }
    else if (position == list->size - 1)
    {
        return linked_list_remove_last(list);
    }
    else
    {
        node = list->head;

        for (uint32_t i = 0; i < position; i++)
        {
            node = node->next;
        }

        node->prev->next = node->next;
        node->next->prev = node->prev;
        free(node->value);
        free(node);
        list->size--;
    }

    return true;
}

void *linked_list_get_first(const linkedList_t *list)
{
    if ((list == NULL) || (list->size == 0))
    {
        return NULL;
    }

    return list->head->value;
}

void *linked_list_get_last(const linkedList_t *list)
{
    if ((list == NULL) || (list->size == 0))
    {
        return NULL;
    }

    return list->tail->value;
}

void *linked_list_get_at(const linkedList_t *list, uint32_t position)
{
    if ((list == NULL) || (list->size == 0) || (position >= list->size))
    {
        return NULL;
    }

    if (position == 0)
    {
        return list->head->value;
    }
    if (position == list->size - 1)
    {
        return list->tail->value;
    }

    linkedListNode_t *node = list->head;
    for (uint32_t i = 0; i < position; i++)
    {
        node = node->next;
    }

    return node->value;
}

linkedList_t *linked_list_create(size_t size)
{
    linkedList_t *list = (linkedList_t *) malloc(sizeof(linkedList_t));

    if ((list == NULL) || (size == 0))
    {
        return NULL;
    }

    list->data_size = size;
    list->head = NULL;
    list->tail = NULL;
    list->size = 0;

    return list;
}

void linked_list_destroy(linkedList_t *list)
{
    if (list == NULL)
    {
        return;
    }

    while (list->head != NULL)
    {
        linked_list_remove_first(list);
    }

    free(list);
    list = NULL;
}

#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include <stdint.h>
#include <stdbool.h>

typedef struct linkedListNode_t linkedListNode_t;
struct linkedListNode_t
{
    void *value;
    linkedListNode_t *next;
    linkedListNode_t *prev;
};

typedef struct
{
    linkedListNode_t *head;
    linkedListNode_t *tail;
    uint32_t size;
    size_t data_size;
} linkedList_t;

/*
* @brief adds an element to the end of the list
*
* @param *list pointer to the list
* @param *element pointer to element whose value will be stored
*
* @return true if element is added successfully
*/
bool linked_list_insert_last(linkedList_t *list, const void *element);

/*
* @brief adds an element to the beginning of the list
*
* @param *list pointer to the list
* @param *element pointer to element whose value will be stored
*
* @return true if element is added successfully
*/
bool linked_list_insert_first(linkedList_t *list, const void *element);

/*
* @brief adds an element to the specific position in the list
*
* @param *list pointer to the list
* @param position position in the list where element will be placed
* @param *element pointer to element whose value will be stored
*
* @return true if element is added successfully
*/
bool linked_list_insert_at(linkedList_t *list, const void *element, uint32_t position);

/*
* @brief removes the last element of the list
*
* @param *list pointer to the list
*
* @return true if element is removed successfully
*/
bool linked_list_remove_last(linkedList_t *list);

/*
* @brief removes the first element of the list
*
* @param *list pointer to the list
*
* @return true if element is removed successfully
*/
bool linked_list_remove_first(linkedList_t *list);

/*
* @brief removes the first occurrence of an element in the list
*
* @param *list pointer to the list
* @param *element pointer to an element with value that should be removed
*                 from the list
*
* @return true if element is removed successfully
*/
bool linked_list_remove_element(linkedList_t *list, void *element);

/*
* @brief removes an element from specific position in the list
*
* @param *list pointer to the list
* @param position position in the list where element will be removed from
*
* @return true if element is removed successfully
*/
bool linked_list_remove_at(linkedList_t *list, uint32_t position);

/*
* @brief get the first element of the list
*
* @param *list pointer to the list
*
* @return element or NULL if a list is empty
*/
void *linked_list_get_first(const linkedList_t *list);

/*
* @brief get the last element of the list
*
* @param *list pointer to the list
*
* @return element or NULL if a list is empty
*/
void *linked_list_get_last(const linkedList_t *list);

/*
* @brief get the element from specific position in the list
*
* @param *list pointer to the list
* @param position position of an element in a list
*
* @return element if exists, otherwise NULL
*/
void *linked_list_get_at(const linkedList_t *list, uint32_t position);

/*
* @brief Initializes a list
*
* @param size size of data type that will be stored in the list
*
* @return pointer to the list if allocation is successful,
*         NULL otherwise
*/
linkedList_t *linked_list_create(size_t size);

/*
* @brief Deletes the list and its elements
*
* @param *list pointer to the list
*/
void linked_list_destroy(linkedList_t *list);

#endif // LINKED_LIST_H
